package main

import (
	"fmt"
	"time"

	. "github.com/logrusorgru/aurora"
)

func main() {
	var i = 300000001
	var d int

	for {
		i = i + 2
		start := time.Now()
		d = 2
		for d < i {
			if i%d == 0 {
				break
			}
			d++
			if d > i/2 {
				elapsed := time.Since(start)
				fmt.Println(i, "PRIME - DURATION:", Green(elapsed))
				break
			}
		}
	}
}
